
package com.mycompany.ex4.teste;

import com.mycompany.ex4.CalculadoraDeComissao;
import com.mycompany.ex4.Venda;
import static org.junit.Assert.*;
import org.junit.Test;


public class CalculadoraDeComissaoTest {
    
@Test
    public void deveCalcularComissao(){
    
    CalculadoraDeComissao calculadora = new CalculadoraDeComissao();
    
    double resultado = calculadora.calcular(new Venda(25, 8));
    assertEquals(10, resultado, 0.001);
    
    
}
}
